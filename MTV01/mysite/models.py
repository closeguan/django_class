from django.db import models

# Create your models here.
class NewTable(models.Model):
    models_f = models.BigIntegerField()
    bool_f = models.BooleanField()
    date_f = models.DateField(auto_now=True)
    char_f = models.CharField(max_length=20,unique=True)
    datetime_f = models.DateTimeField(auto_now_add=True)
    decimal_f = models.DecimalField(max_digits=10,decimal_places=2)
    float_f = models.FloatField(null=True)
    int_f = models.IntegerField(default=2010)
    tesxt_f = models.TextField()

    def __str__(self):
        return self.char_f     # 要定義在 NewTable 這個 class 

class Product(models.Model):
    SIZES = (
        ('S', 'Small'),
        ('M', 'Medium'),
        ('L', 'Large'),
    )
    #sku = models.CharField(max_length=15)
    sku = models.CharField(max_length=15,unique=True)  # unique 可以避免有重複的 sku (產品編號)
    name = models.CharField(max_length=15)
    price = models.PositiveIntegerField()
    size = models.CharField(max_length=1, choices=SIZES)
    qty = models.PositiveIntegerField()

    def __str__(self):
        return self.name

