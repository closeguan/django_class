from django.contrib import admin
#寫法1
from .models import Product, NewTable
#寫法2
#from mysite.models import Product, NewTable 

# Register your models here.

class ProductAdmin(admin.ModelAdmin):
    list_display = ('sku','name','price','qty')


class NewTableAdmin(admin.ModelAdmin):
    list_display = ('char_f','date_f','datetime_f')


#admin.site.register(NewTable)
#admin.site.register(Product)
admin.site.register(Product, ProductAdmin)
admin.site.register(NewTable, NewTableAdmin)
