from django.db import models


# Create your models here. 可以參考 06 Django 關聯式資料庫 ppt p.8 的關聯是圖表來呈現下面的欄位關係
class Maker(models.Model):  #手機品牌資訊
    name = models.CharField(max_length=30)
    country = models.CharField(max_length=10)
    
    def __str__(self):
        return self.name


class PModel(models.Model):  #手機型號 
    maker = models.ForeignKey(Maker, on_delete=models.CASCADE) # 當參照物件被刪除，此參照物件也會被刪除喔~  [ex]: 就如刪除"製造商"，此廠牌的"手機們"也都會一起刪除喔
    name = models.CharField(max_length=40)
    url = models.URLField(default='https://www.sogi.com.tw/')  #預設連道手機王的網頁
    
    def __str__(self):
        return self.name

class Product(models.Model):  #商店手機產品資訊
    pmodel = models.ForeignKey(PModel, on_delete=models.CASCADE, verbose_name='型號') 
    nickname = models.CharField(max_length=30, default='超值二手機', verbose_name='摘要')
    description = models.TextField(default='暫無說明')
    year = models.PositiveIntegerField(default=2016, verbose_name='出廠年份')
    price = models.PositiveIntegerField(default=0,  verbose_name='價格')
    
    def __str__(self):
        return self.nickname
    

class PPhoto(models.Model):
    product = models.ForeignKey(Product, on_delete=models.CASCADE)
    description = models.CharField(max_length=20, default='產品照片')
    url = models.URLField(default='http://i.imgur.com/Z230eeq.png')
    
    def __str__(self):
        return self.description
