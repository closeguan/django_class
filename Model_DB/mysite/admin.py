from django.contrib import admin
from mysite import models
# Register your models here.
admin.site.register(models.Maker)
admin.site.register(models.PModel)

admin.site.register(models.PPhoto)


class ProductAdmin(admin.ModelAdmin):
    list_display=('pmodel', 'nickname', 'price', 'year')  #  要顯示的欄位
    search_fields=('nickname',)  #  新增搜尋功能
    ordering = ('-price', )      #  排序

admin.site.register(models.Product, ProductAdmin)


