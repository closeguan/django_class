# Generated by Django 2.2.7 on 2019-12-01 00:35

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('mysite', '0003_auto_20191130_1326'),
    ]

    operations = [
        migrations.AlterField(
            model_name='product',
            name='nickname',
            field=models.CharField(default='超值二手機', max_length=30, verbose_name='摘要'),
        ),
        migrations.AlterField(
            model_name='product',
            name='pmodel',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='mysite.PModel', verbose_name='型號'),
        ),
        migrations.AlterField(
            model_name='product',
            name='price',
            field=models.PositiveIntegerField(default=0, verbose_name='價格'),
        ),
        migrations.AlterField(
            model_name='product',
            name='year',
            field=models.PositiveIntegerField(default=2016, verbose_name='出廠年份'),
        ),
    ]
