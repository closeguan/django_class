from django.shortcuts import render
from datetime import datetime
# Create your views here.

#def index(request):
#    return render(request, 'index.html', {'msg':'Hello'})



def about(request):
    msg = 'Hello'
    now = datetime.now()
    return render(request, 'about.html', locals())



def index(request, tvno = 6):
    tv_list = [
        {'name':'東森直播', 'tvcode':'RaIJ767Bj_M'},
        {'name':'吉娃娃', 'tvcode':'h09mrBcmiyY'},
        {'name':'哈哈台', 'tvcode':'bxVbNyHKADE'},
        {'name':'公共電視台', 'tvcode':'EoL5Q6xnXVM#action=share'},
        {'name':'TVBS', 'tvcode':'Hu1FkdAOws0'},
        {'name':'中視', 'tvcode':'XBne4oJGEhE'},
        {'name':'冰雪奇緣2', 'tvcode':'d_zVfl4bpJI'},
        {'name':'國瑜草包', 'tvcode':'ISo3m0RxmSw'}
    ]
    now = datetime.now()
    tvno = tvno
    tv = tv_list[tvno]
    hour = now.timetuple().tm_hour
    return render(request, 'tv.html', locals())


def engtv(request, tvno='0'):
    tv_list = [
    {'name':'SkyNews', 'tvcode':'99U4CH_k5F0'},
    {'name':'Euro News', 'tvcode':'SM29tFvd_VM'},
    {'name':'India News', 'tvcode':'E7dbhET6_EA'},
    {'name':'CCTV', 'tvcode':'vCDDYb_M2B4'},
    {'name':'Señorita', 'tvcode':'Pkh8UtuejGw'},
    ]
    now = datetime.now()
    tvno = tvno
    tv = tv_list[int(tvno)]
    return render(request, 'engtv.html', locals())

def carlist(request, maker=0):
    car_maker = ['SAAB', 'Ford', 'Honda', 'Mazda', 'Nissan','Toyota' ]
    car_list = [ [],
    ['Fiesta', 'Focus', 'Modeo', 'EcoSport', 'Kuga', 'Mustang'],
    ['Fit', 'Odyssey', 'CR-V', 'City', 'NSX'],
    ['Mazda3', 'Mazda5', 'Mazda6', 'CX-3', 'CX-5', 'MX-5'],
    ['Tida', 'March', 'Livina', 'Sentra', 'Teana', 'X-Trail', 'Juke', 'Murano'],
    ['Camry','Altis','Yaris','86','Prius','Vios', 'RAV4', 'Wish']
    ]
    maker = maker
    maker_name = car_maker[maker]
    cars = car_list[maker]
    return render(request, 'carlist.html', locals())

def carprice(request, maker=0):
    car_maker = ['Ford', 'Honda', 'Mazda']
    car_list = [[{'model':'Fiesta', 'price': 203500, 'qty': 8},{'model':'Focus','price': 605000, 'qty': 6},{ 'model':'Mustang','price': 410000,  'qty': 2}],[{'model':'Fit', 'price': 450000, 'qty': 5},{'model':'City', 'price': 150000, 'qty': 7},{'model':'NSX', 'price':1200000, 'qty': 3}],[   {'model':'Mazda3', 'price': 329999, 'qty': 7},{'model':'Mazda5', 'price': 603000, 'qty': 1},{'model':'Mazda6', 'price':850000, 'qty': 5}],]
    maker = maker
    maker_name = car_maker[maker]
    cars = car_list[maker]
    return render(request, 'carprice.html', locals())

