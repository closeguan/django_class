from django.shortcuts import render
from django.http import HttpResponse
from django.urls import reverse    # 使用Python在views.py的寫法
# Create your views here.

def homepage(request):   # url  改了  也要在這邊定義 要帶入的變數
    year = 2019
    month = 10
    day = 30
    postid=3
    html = "<a href='{}'>Show the Post</a>" \
    .format(reverse('post-url', args=(year, month, day, postid,)))
    return HttpResponse(html)

def post2(request, yr, mon, day, post_num):
    return render(request, 'post2.html', locals()) 


def about(request, author_no=0):   # 給預設值，預設url對應 about 頁面時，後面不給輸入值，給他預設時就不會報錯
    html = "<h2>Here is No:{}'s about page!</h2><hr>".format(author_no)
    return HttpResponse(html)


def listing(request, yr, mon, day):
    html = "<h2>List Date is {}/{}/{}</h2><hr>".format(yr, mon, day)
    return HttpResponse(html)


def post(request, yr, mon, day, post_num):
    html = "<h2>{}/{}/{}:Post Number:{}</h2><hr>".format(yr, mon, day, post_num)
    return HttpResponse(html)


def company(request):
    return HttpResponse("My Company")

def sales(request):
    return HttpResponse("My sales")

def contact(request):
    return HttpResponse("My contact")


